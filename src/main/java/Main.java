import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        port(8008);
        get("/hello", (req, res) -> "Hello World");
    }
}